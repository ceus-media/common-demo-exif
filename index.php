<?php
(@include 'vendor/autoload.php') or die('Please use composer to install required packages.' . PHP_EOL);
error_reporting( E_ALL );
new UI_DevOutput;

$filename	= 'test.jpg';

$image		= new UI_Image_Exif( $filename );
$data		= $image->getRawData();

$body	= '
<div class="container">
	<h2><a class="muted">CeusMedia::Common</a> EXIF Demo</h2>
	<div class="row-fluid">
		<div class="span7">
			<dl>
				<dt>Beschreibung</dt>
				<dd>'.$data['ImageDescription'].'</dd>
				<dt>Fotograph</dt>
				<dd>'.$data['Artist'].'</dd>
				<dt>Copyright</dt>
				<dd>'.$data['Copyright'].'</dd>
				<dt>Kamera</dt>
				<dd>'.$data['Model'].'</dd>
				<dt>Belichtungszeit</dt>
				<dd>'.$data['ExposureTime'].'</dd>
				<dt>ISO-Wert</dt>
				<dd>'.$data['ISOSpeedRatings'].'</dd>
			</dl>
		</div>
		<div class="span4 offset1">
			<img src="'.$filename.'" class="thumbnail"/>
		</div>
	</div>
	<h3>Source Code</h3>
	<pre class="pre-scrollable">
$image		= new UI_Image_Exif( $filename );
$data		= $image->getRawData();
print_m( $data );
	</pre>
	<h3>Resulting Data</h3>
	'.print_m( $image->getRawData(), NULL, NULL, TRUE ).'
</div>';

$page	= new UI_HTML_PageFrame();
$page->addStylesheet( 'https://cdn.ceusmedia.de/css/bootstrap.min.css' );
$page->addBody( $body );
print( $page->build() );
